# Pokemon App

The goal of this exercise is to build a Pokemon React app that allows us to see and interact with a list of Pokemon.

The project currently contains a basic working skeleton made with `create-react-app` that we will use as a base. 

The [Pokemon API](https://pokeapi.co/) will be the data source.

## Goals

We want to show a list of the first 20 Pokemon from the API, with their name and image.

We also want to be able to switch between a single-column and a grid layout.

We want to be able to click the items on the list to show the Pokemon details.

Optionally, if time allows, we want to be able to add pagination to the list.
